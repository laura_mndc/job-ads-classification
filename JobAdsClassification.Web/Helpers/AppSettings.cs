﻿
namespace JobAdsClassification.Web.Helpers
{
    public class AppSettings
    {
        public string ApiKey { get; set; }
        public string BaseAddress { get; set; }
    }
}
