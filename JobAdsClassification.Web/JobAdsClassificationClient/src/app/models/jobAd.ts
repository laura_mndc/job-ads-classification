export class JobAd {
  public title: string;
  public location: string;
  public department: string;
  public companyProfile: string;
  public description: string;
  public requirements: string;
  public benefits: string;
  public telecommuting: boolean;
  public hasCompanyLogo: boolean;
  public hasQuestions: boolean;
  public employmentType: string;
  public requiredExperience: string;
  public requiredEducation: string;
  public industry: string;
  public function: string;

  constructor(
    title: string,
    location: string,
    department: string,
    companyProfile: string,
    description: string,
    requirements: string,
    benefits: string,
    telecommuting: boolean,
    hasCompanyLogo: boolean,
    hasQuestions: boolean,
    employmentType: string,
    requiredExperience: string,
    requiredEducation: string,
    industry: string,
    jobFunction: string
  ) {
    this.title = title;
    this.location = location;
    this.department = department;
    this.companyProfile = companyProfile;
    this.description = description;
    this.requirements = requirements;
    this.benefits = benefits;
    this.telecommuting = telecommuting;
    this.hasCompanyLogo = hasCompanyLogo;
    this.hasQuestions = hasQuestions;
    this.employmentType = employmentType;
    this.requiredExperience = requiredExperience;
    this.requiredEducation = requiredEducation;
    this.industry = industry;
    this.function = jobFunction;
  }
}
