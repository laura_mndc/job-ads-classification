﻿
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using JobAdsClassification.Web.Models;
using JobAdsClassification.Web.Services.Interfaces;
using JobAdsClassification.Web.Helpers;
using Newtonsoft.Json.Linq;

namespace JobAdsClassification.Web.Services.JobService
{
    public class JobAdsClassificationService : IJobAdsClassificationService
    {
        private readonly ConfigurationProvider configurationProvider;

        public JobAdsClassificationService(ConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public async Task<int> DetectFraudulence(JobAd ad)
        {
            using (var client = new HttpClient())
            {

                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, List<Dictionary<string, string>>>() {
                        {
                            "input1",
                            new List<Dictionary<string, string>>(){new Dictionary<string, string>(){                                            
                                            {
                                                "title", ad.Title
                                            },
                                            {
                                                "location",ad.Location
                                            },
                                            {
                                                "department", ad.Department
                                            },
                                            {
                                                "company_profile", ad.CompanyProfile
                                            },
                                            {
                                                "description", ad.Description
                                            },
                                            {
                                                "requirements", ad.Requirements
                                            },
                                            {
                                                "benefits", ad.Benefits
                                            },
                                            {
                                                "telecommuting", Convert.ToInt32(ad.Telecommuting).ToString()
                                            },
                                            {
                                                "has_company_logo", Convert.ToInt32(ad.HasCompanyLogo).ToString()
                                            },
                                            {
                                                "has_questions", Convert.ToInt32(ad.HasQuestions).ToString() 
                                            },
                                            {
                                                "employment_type", ad.EmploymentType
                                            },
                                            {
                                                "required_experience", ad.RequiredExperience
                                            },
                                            {
                                                "required_education", ad.RequiredEducation
                                            },
                                            {
                                                "industry", ad.Industry
                                            },
                                            {
                                                "function", ad.Function
                                            },
                                }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };

                string apiKey = configurationProvider.AppSettings.ApiKey; 
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
                client.BaseAddress = new Uri(configurationProvider.AppSettings.BaseAddress);


                HttpResponseMessage response = await client
                                                    .PostAsync("", new StringContent(JsonConvert.SerializeObject(scoreRequest), Encoding.UTF8, "application/json"))
                                                    .ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    dynamic data = JObject.Parse(result);
                    int fraudulent = Convert.ToInt32(data.Results.fraudulent[0]["Scored Labels"]);
                    return fraudulent;
                }
                else
                {
                    return (int)response.StatusCode;
                }
            }
        }
    }
}
