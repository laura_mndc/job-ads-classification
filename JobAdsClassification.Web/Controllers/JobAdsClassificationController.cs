﻿using JobAdsClassification.Web.Models;
using JobAdsClassification.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace JobAdsClassification.Web.Controllers
{
    [ApiController]
    [Route("api/job-ad")]
    public class JobAdsClassificationController : ControllerBase
    {  

        private IJobAdsClassificationService jobService;

        public JobAdsClassificationController(IJobAdsClassificationService jobService)
        {
            this.jobService = jobService;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] JobAd model)
        {
            // Call the web service
            int result = await jobService.DetectFraudulence(model);

            if (result < 2)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }         
           
        }
    }
}
