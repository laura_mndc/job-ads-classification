import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ResultDialogData } from '../models/result-dialog-info';

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html',
  styleUrls: ['./result-dialog.component.scss'],
})
export class ResultDialogComponent implements OnInit {
  fraudulence: boolean;
  constructor(
    private dialogRef: MatDialogRef<ResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ResultDialogData
  ) {
    this.fraudulence = this.data.fraudulence;
  }

  ngOnInit(): void {}
  public onClick() {
    this.dialogRef.close();
  }
}
