﻿using Microsoft.Extensions.Options;

namespace JobAdsClassification.Web.Helpers
{
    public class ConfigurationProvider
    {
        public AppSettings AppSettings { get; set; }
        public ConfigurationProvider(IOptions<AppSettings> appSettings)
        {
            AppSettings = appSettings.Value;
        }

 
    }
}
