﻿using JobAdsClassification.Web.Models;
using System.Threading.Tasks;

namespace JobAdsClassification.Web.Services.Interfaces
{
    public interface IJobAdsClassificationService
    {
        Task<int> DetectFraudulence(JobAd ad);
    }
}
