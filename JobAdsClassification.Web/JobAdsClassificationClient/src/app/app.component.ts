import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { first } from 'rxjs/operators';
import { JobAd } from './models/jobAd';
import { ResultDialogData } from './models/result-dialog-info';
import { ResultDialogComponent } from './result-dialog/result-dialog.component';
import { JobAdService } from './services/job-ad.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  public formGroup!: FormGroup;
  public errorCode: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private adService: JobAdService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.formGroup = this.formBuilder.group({
      title: ['', [Validators.required]],
      location: ['', [Validators.required]],
      department: ['', [Validators.required]],
      companyProfile: ['', [Validators.required]],
      requirements: ['', [Validators.required]],
      industry: ['', [Validators.required]],
      benefits: ['', [Validators.required]],
      function: ['', [Validators.required]],
      employmentType: ['', [Validators.required]],
      requiredExperience: ['', [Validators.required]],
      requiredEducation: ['', [Validators.required]],
      description: ['', [Validators.required]],
      telecommuting: [false, []],
      hasCompanyLogo: [false, []],
      hasQuestions: [false, []],
    });
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      return;
    }

    let model: JobAd = this.formGroup.value;

    this.adService
      .checkJobAd(model)
      .pipe(first())
      .subscribe(
        (fraudulent) => {
          this.openResultDialog(fraudulent);
        },
        (error) => {
          this.errorCode = error.error;
        }
      );
  }

  public toggle(type: string) {
    this.formGroup.value[type] = !this.formGroup.value[type];
  }

  public getErrorMessage(
    control: AbstractControl,
    controlName: string
  ): string {
    if (control.errors?.['required']) {
      return `${controlName} is required`;
    }
    return '';
  }

  public openResultDialog(fraudulence: boolean) {
    var dialogData: ResultDialogData = {
      fraudulence: fraudulence,
    };

    const dialogRef = this.dialog.open(ResultDialogComponent, {
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe(() => {
      this.initForm();
    });
  }
}
