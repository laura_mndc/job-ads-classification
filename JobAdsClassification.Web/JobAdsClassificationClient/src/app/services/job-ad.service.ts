import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JobAd } from '../models/jobAd';

@Injectable({
  providedIn: 'root',
})
export class JobAdService {
  private serviceBaseUrl = '';

  constructor(
    @Inject('API_BASE_URL') public baseUrl: string,
    private httpClient: HttpClient
  ) {
    this.serviceBaseUrl = `${this.baseUrl}api/job-ad`;
  }

  public checkJobAd(model: JobAd): Observable<boolean> {
    return this.httpClient.post<boolean>(`${this.serviceBaseUrl}`, model);
  }
}
