﻿


namespace JobAdsClassification.Web.Models
{
    public class JobAd
    {
        public string Title { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public string CompanyProfile { get; set; }
        public string Description { get; set; }
        public string Requirements { get; set; }
        public string Benefits { get; set; }
        public bool Telecommuting { get; set; }
        public bool HasCompanyLogo { get; set; }
        public bool HasQuestions { get; set; }
        public string EmploymentType { get; set; }
        public string RequiredExperience { get; set; }
        public string RequiredEducation { get; set; }
        public string Industry { get; set; }
        public string Function { get; set; }

    }
}
